var timer = null;
var play_state = 0;
var sound = true;

$(document).ready(function() {

  /** Function to start the metronome */
  var start = function() {
    if(play_state == 0) {
      play_state = 1;
      $(".beat").removeClass("label-danger").addClass("label-default");
      $(".tacket .panel").removeClass("panel-success").addClass('panel-default');

      $("#start").toggleClass("disabled");
      $("#stop").toggleClass("disabled");
      step(0);
    }
  }

  /** Function to stop the metronome */
  var stop = function() {
    if(play_state == 1) {
      play_state = 0;
      $("#start").toggleClass("disabled");
      $("#stop").toggleClass("disabled");
      clearTimeout(timer);
    }
  }

  /** Function to step to the next tacket */
  var step = function(pos) {
    $previous = $('.tacket[data-pos="' + (pos-1) + '"]');
    $element = $('.tacket[data-pos="' + pos + '"]');

    // scroll to position
    $('html, body').animate({ scrollTop: $element.offset().top }, 500);

    // get the panels
    $panel = $element.children('.panel');
    $prevpanel = $previous.children('.panel');

    $delay = $element.data("duration");
    $beats = $element.data("beats");
    $metro_delay = $delay / $beats;

    $prevpanel.removeClass("panel-success").addClass('panel-default');
    $prevpanel.find(".beat").addClass("label-default").removeClass("label-danger");

    if(!$element.length) return;
    $panel.removeClass("panel-default").addClass('panel-success');

    // clicks
    if(sound) document.getElementById('metro-high').play();
    $panel.find('.beat[data-beat="0"]').addClass("label-danger").removeClass("label-default");

    var timeout = 0;
    for(var i = 1; i < $beats; i++) {
      timeout += $metro_delay;
      var delay = timeout;
      setTimeout(function(pos) {
        $panel.find('.beat[data-beat="' + (pos-1) + '"]').addClass("label-default").removeClass("label-danger");
        $panel.find('.beat[data-beat="' + (pos) + '"]').addClass("label-danger").removeClass("label-default");
        if(sound) document.getElementById('metro-low').play();
      }, delay, i);
    }

    // tackets
    timer = setTimeout(function() { 
      step(pos+1);
    }, $delay);
  }

  // controller delegates
  $("#start").click(function() { start(); });
  $("#stop").click(function() { stop(); });
  $("#mute").click(function() {
    sound = !sound;
    $("#mute span").toggleClass("glyphicon-volume-off").toggleClass("glyphicon-volume-up");
  });
  $("#edit").click(function() {
    stop();
    $('html, body').animate({ scrollTop: $("#settings").offset().top }, 500);
  });
});
