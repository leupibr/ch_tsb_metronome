<?php
  require_once("functions.php");

  // read the configuration file.
  $config = json_decode(file_get_contents("config.json"));

  // check posted data and split into variables.
  $selectedKeys = array_values(preg_grep_keys('/^' . $config->keys->prefix . '\d+$/', $_POST));
  $selectedSpeed = array_values(preg_grep_keys('/^' . $config->speed->prefix . '\d+$/', $_POST));
  $selectedSignature = array_values(preg_grep_keys('/^' . $config->signature->prefix . '\d+$/', $_POST));
  $selectedStyle = array_values(preg_grep_keys('/^' . $config->style->prefix . '\d+$/', $_POST));

  // check values and fill "needed" values
  if(!(empty($selectedKeys) && empty($selectedSpeed) && empty($selectedSignature) && empty($selectedStyle))) {
    if(empty($selectedSpeed)) $selectedSpeed = array("100");
    if(empty($selectedSignature)) $selectedSignature = array("4/4");
  }
?>

<html>
<head>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">
  <link rel="stylesheet" href="css/style.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
  <?php if(!(empty($selectedKeys) && empty($selectedSpeed) && empty($selectedSignature) && empty($selectedStyle))) {
    printTackets($selectedKeys, $selectedSpeed, $selectedSignature, $selectedStyle, $config->tackets);
    printControls($config->primarySound, $config->secondarySound);
  } ?>

  <div id="settings" class="container">
    <form method="POST">
      <?php printPanel("Keys", $config->keys->prefix, $config->keys->options); ?>
      <?php printPanel("Speed", $config->speed->prefix, $config->speed->options); ?>
      <?php printPanel("Signature", $config->signature->prefix, $config->signature->options); ?>
      <?php printPanel("Style", $config->style->prefix, $config->style->options); ?>
      <input class="btn btn-default pull-right" type="submit"/>
    </form>
  </div>

  <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
  <script src="js/script.js"></script>
</body>

</html>
