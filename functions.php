<?php

////////// Utility funcitons //////////

/**
 * Return array entries that keys match the pattern.
 * @param string $pattern The pattern to search for, as a string.
 * @param array $input The input array.
 * @param int If set to <c>PREG_GREP_INVERT</c>, this function returns the elements of the input array that do not match the given pattern.
 * @return array Returns the array consisting of the elements of the input array that keys match the given pattern.
 */
function preg_grep_keys($pattern, $input, $flags = 0) {
  return array_intersect_key($input, array_flip(preg_grep($pattern, array_keys($input), $flags)));
}

////////// HTML related functions //////////

/**
 * Prints a bootstrap panel.
 */
function printPanel($name, $prefix, $values) {
  $sz_xs = 2; $sz_sm = 1;

  if(count($values) <= 6) {
    $sz_xs = $sz_sm = floor(12/count($values));
  }

  echo '<div class="panel panel-default">';
  echo '<div class="panel-heading"><h3 class="panel-title">' . $name . '</h3></div>';
  echo '<div class="panel-body" data-toggle="buttons">';
  $i = 0;
  foreach($values as $value) {
    echo '<label class="btn btn-primary col-xs-' . $sz_xs . ' col-sm-' . $sz_sm . (isset($_POST[$prefix . $i]) ? ' active' : '') . '">';
    echo '<input type="checkbox" name="' . $prefix . $i . '" value="' . $value . '" ' . (isset($_POST[$prefix . $i]) ? 'checked' : '') . '>';
    echo $value;
    echo '</label>';
    $i++;
  }
  echo '</div>';
  echo '</div>';
}

/**
 * Prints the tackets.
 * @param array $keys The keys to print randomly.
 * @param array $speeds The speeds to print randomly.
 * @param array $signatures The signatures to print randomly.
 * @param array $styles The styles to print randomly.
 * @param int $num The number of tackets to print.
 */
function printTackets($keys, $speeds, $signatures, $styles, $num) {
  echo '<div class="container">';
  for($pos = 0; $pos < $num; $pos++) {

    // set keys random index
    $keysIdx = rand(0, count($keys)-1);

    // set speed, signature and style only each 4th repetition
    $speedIdx = $pos % 4 == 0 ? rand(0, count($speeds)-1) : $speedIdx;
    $signatureIdx = $pos % 4 == 0 ? rand(0, count($signatures)-1) : $signatureIdx;
    $styleIdx = $pos % 4 == 0 ? rand(0, count($styles)-1) : $styleIdx;

    // get signature and its "weight"
    $signature = $signatures[$signatureIdx];
    $signatureWeight = eval("return " . $signature . ";");

    // calculate duration of tacket and number of beats per tacket
    $duration = 60000/$speeds[$speedIdx] * ($signatureWeight*4);
    $beats = explode('/', $signature)[0];

    echo '<div class="tacket col-xs-6 col-sm-3" data-pos="' . $pos . '" data-duration="' . $duration . '" data-beats="' . $beats . '">';
    echo '<div class="panel panel-' . ($pos === 0 ? 'success' : 'default') . '">';
    echo '<div class="panel-heading">';
    echo '<div class="badge">' . ($pos+1) . '</div>';
    if(!empty($speeds)) { echo '<div class="label label-danger pull-right speed">' . $speeds[$speedIdx]. '</div>'; }
    echo '</div>';

    echo '<div class="panel-body">';
    if(!empty($keys)) { echo '<h1>' . $keys[$keysIdx]. '</h1>'; }
    for($i = 0; $i < $beats; $i++) {
      echo '<div class="label label-default beat" data-beat="' . $i . '">&nbsp;</div> ';
    }
    if(!empty($signatures)) { echo '<div class="label label-primary pull-right style">' . $signature. '</div>'; }
    if(!empty($styles)) { echo '<div class="label label-primary pull-right style">' . $styles[$styleIdx]. '</div>'; }
    echo '</div>';
    echo '</div>';
    echo '</div>';
  }
  echo '</div>';
}

/**
 * Prints the controls
 */
function printControls($primarySound, $secondarySound) {
  echo '<div class="controls">';
  echo '<audio id="metro-high" src="' . $primarySound . '" preload="auto"></audio>';
  echo '<audio id="metro-low" src="' . $secondarySound . '" preload="auto"></audio>';
  echo '<a id="start" class="btn btn-primary"><span class="glyphicon glyphicon-play"></span></a>';
  echo '<a id="stop" class="btn btn-primary disabled"><span class="glyphicon glyphicon-stop"></span></a>';
  echo '<br>';
  echo '<a id="mute" class="btn btn-primary"><span class="glyphicon glyphicon-volume-up"></span></a>';
  echo '<a id="edit" class="btn btn-primary"><span class="glyphicon glyphicon-edit"></span></a>';
  echo '</div>';
}
?>
