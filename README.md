# Jamtronome
![Version](https://img.shields.io/badge/version-V1.0.0-green.svg)
[![License](https://img.shields.io/badge/license-MIT-green.svg)](http://opensource.org/licenses/MIT)
[![Language](https://img.shields.io/badge/language-PHP%205.5-blue.svg)](http://php.net/)

## About
This project was an idea me and my band had. We would like to warm up together but just playing the same thing everytime is not effective and changing keys without plan a little bit hard. So I decided to write a generator for random keys and speeds that can be used for a warm up.

## License
MIT
